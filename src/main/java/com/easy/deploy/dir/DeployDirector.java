package com.easy.deploy.dir;



import com.easy.annotation.IfMapper;
import com.easy.annotation.find.ReturnObject;
import com.easy.deploy.abs.DeployBuilder;
import com.easy.deploy.util.LogUtil;
import com.easy.deploy.vo.DeployConnect;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@IfMapper(pack = "com.easy.deploy.impl")
@Component
public class DeployDirector {

    DeployBuilder deployBuilder;

    @Async
    public void start(String tag, DeployConnect deployConnect) {
        this.deployBuilder = ReturnObject.getObj(DeployDirector.class,tag);;
        this.deployBuilder.setDeployConnect(deployConnect);
        try {
            deployBuilder.mavenClean();
            deployBuilder.mavenInstall();
            deployBuilder.npmBuild();
            deployBuilder.processNumber();
            deployBuilder.closeProcess();
            deployBuilder.delete();
            deployBuilder.initDeployPath();
            deployBuilder.mkdir();
            deployBuilder.uploading();
            deployBuilder.run();
            deployBuilder.saveDeployRecord();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
